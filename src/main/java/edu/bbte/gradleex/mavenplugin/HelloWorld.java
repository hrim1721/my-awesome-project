package edu.bbte.gradleex.mavenplugin;

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello " + System.getProperty("name", "stranger"));
    }
}