package edu.bbte.idde.hrim1721.frontend;

import edu.bbte.idde.hrim1721.backend.Menu;

import javax.swing.*;
import java.awt.*;

public final class Main extends JFrame {
    private JTable table;

    private Main() {
        super();
        initScreen();
    }

    private void initScreen() {
        JButton loadMenuBtn;
        JPanel panel = new JPanel();
        this.setContentPane(panel);

        loadMenuBtn = new JButton();
        loadMenuBtn.setText("Load Menu");

        loadMenuBtn.addActionListener(e -> {
            Menu menu = new Menu();

            String[] columnNames = {
                    "Id", "Name", "Quality", "Price", "Chef"
            };

            table = new JTable(menu.getMeals(), columnNames);
            JScrollPane scrollPane = new JScrollPane(table);
            table.setFillsViewportHeight(true);
            panel.add(scrollPane);
            panel.getComponent(0).setVisible(false);
        });
        loadMenuBtn.setVisible(true);
        panel.add(loadMenuBtn);
    }

    public static void main(String[] args) {
        JFrame jframe;
        jframe = new Main();
        jframe.setSize(500, 500);
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jframe.setVisible(true);
    }

}
