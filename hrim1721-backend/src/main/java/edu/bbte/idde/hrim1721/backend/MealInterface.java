package edu.bbte.idde.hrim1721.backend;

interface MealInterface {
    String[][] getMeals();

    void insertMeal(Meal meal);
}
