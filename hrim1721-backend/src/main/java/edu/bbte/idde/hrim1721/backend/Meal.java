package edu.bbte.idde.hrim1721.backend;

public class Meal {
    private int id;
    private String name;
    private int quality;
    private int price;
    private String chef;

    public Meal(int id, String name, int quality, int price, String chef) {
        this.id = id;
        this.name = name;
        this.quality = quality;
        this.price = price;
        this.chef = chef;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuality() {
        return this.quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getChef() {
        return this.chef;
    }

    public void setChef(String chef) {
        this.chef = chef;
    }
}
