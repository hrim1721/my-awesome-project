package edu.bbte.idde.hrim1721.backend;

import java.util.List;
import java.util.ArrayList;

public class Menu implements MealInterface {
    private final List<Meal> mealList;

    public Menu() {
        mealList = new ArrayList<>();

        insertMeal(new Meal(1, "Lecso", 5, 15, "Edesanyam"));
        insertMeal(new Meal(2, "Mazsola", 9, 5, "Szolo"));
        insertMeal(new Meal(3, "Puliszka", 1, 10, "Nagyszulok"));
        insertMeal(new Meal(4, "Burger", 8, 30, "California"));
        insertMeal(new Meal(5, "Pizza", 8, 20, "Italiano"));
    }

    @Override
    public String[][] getMeals() {
        String[][] meals = new String[mealList.size()][5];
        int iterator = 0;
        for (Meal meal : mealList) {
            meals[iterator][0] = String.valueOf(meal.getId());
            meals[iterator][1] = meal.getName();
            meals[iterator][2] = String.valueOf(meal.getQuality());
            meals[iterator][3] = String.valueOf(meal.getPrice());
            meals[iterator][4] = meal.getChef();
            iterator++;
        }
        return meals;
    }

    @Override
    public void insertMeal(Meal meal) {
        mealList.add(meal);
    }
}
