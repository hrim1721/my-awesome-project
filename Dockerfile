FROM gradle:jdk8-alpine as build-env
COPY . ./
USER root
RUN cd hrim1721-frontend && gradle jar

FROM openjdk:8-jre-alpine 
copy --from=build-env /home/gradle/hrim1721-frontend/build/libs/hrim1721-frontend-1.0-SNAPSHOT.jar ./
CMD java -jar hrim1721-frontend-1.0-SNAPSHOT.jar